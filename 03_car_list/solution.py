import os
import csv

class CarBase:
    def __init__(self, brand, photo_file_name, carrying):
        self.brand = brand
        self.photo_file_name = photo_file_name
        self.carrying = float(carrying)
    
    def get_photo_file_ext(self):
        return os.path.splitext(self.photo_file_name)[1]


class Car(CarBase):
    def __init__(self, brand, photo_file_name, carrying, passenger_seats_count):
        self.car_type = "car"
        super().__init__(brand, photo_file_name, carrying)
        self.passenger_seats_count = int(passenger_seats_count)


class Truck(CarBase):
    def __init__(self, brand, photo_file_name, carrying, body_whl):
        self.car_type = "truck"
        super().__init__(brand, photo_file_name, carrying)
        
        try:
            sz = [i for i in map(float, body_whl.split('x'))] 
        except ValueError:
            sz = [0.0, 0.0, 0.0]
        
        self.body_length = sz[0]
        self.body_width = sz[1]
        self.body_height = sz[2]

    def get_body_volume():
        return self.body_length * self.body_width * self.body_height


class SpecMachine(CarBase):
    def __init__(self, brand, photo_file_name, carrying, extra):
        self.car_type = "spec_machine"
        super().__init__(brand, photo_file_name, carrying)
        self.extra = extra


def get_car_list(csv_filename):
    #'coursera_week3_cars.csv'
    car_list = []
    with open(csv_filename) as csv_fd:
        reader = csv.reader(csv_fd, delimiter=';')
        next(reader)  # пропускаем заголовок
        for row in reader:
            if row[0] == "car":
                car = Car(row[1], row[3], row[5], row[2])
            elif row[0] == "truck":
                car = Truck(row[1], row[3], row[5], row[4])
            elif row[0] == 'spec':
                # brand, photo_file_name, carrying, extra
                car = Spec(row[1], row[3], row[5], row[6])
            car_list.append(car)

    return car_list



# import csv
# with open('car_list.csv', 'w') as csvfile:
#     wr = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
#     wr.writerow( ['car_type', 'brand', 'passenger_seats_count', 'photo_file_name', 'body_whl', 'carrying', 'extra'] )
#     wr.writerow( ['car', 'Nissan xTrail', '4', 'f1.jpeg', '', '2.5', ''] )
#     wr.writerow( ['truck', 'Man', '', 'f2.png', '8x3x2.5', '20', ''] )
#     wr.writerow( ['spec_machine', 'Hitachi', '', 'f4.jpeg', '', '1.2', 'Легкая техника для уборки снега'] )

# with open('car_list.csv') as csvfile:
#     rd = csv.reader(csvfile, delimiter=';')
#     for row in rd:
#         print(row)

# import pandas as pd
# pd.read_csv('car_list.csv', delimiter=';')

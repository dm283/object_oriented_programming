import sys
import pprint
import requests
from dateutil.parser import parse


class OpenWeatherForecast:

    def get(self, city):
        city_dict = {
            "Voronezh": "472045",
            "Kazan": "551487"
        }
        if city not in city_dict:
            print(f"No data for {city}")
            exit()
        city_id = city_dict[city]
        appid = "a8a949143ca1b43afe204c3a86a8f906"
        res = requests.get("http://api.openweathermap.org/data/2.5/forecast",
                            params={'id': city_id, 'units': 'metric', 'APPID': appid})
        data = res.json()
        forecast = list()
        for i in data['list']:
                if(i['dt_txt'].split()[1]) == "12:00:00":
                    el = {
                        "date": parse(i['dt_txt'].split()[0]),
                        "temp": i['main']['temp']
                    }
                    forecast.append(el)
        return forecast


class CityInfo:

    def __init__(self, city, weather_forecast=None):
        self.city = city
        self._weather_forecast = weather_forecast or OpenWeatherForecast()

    def weather_forecast(self):
        return self._weather_forecast.get(self.city)


def _main(cty):
    city_info = CityInfo(cty)
    forecast = city_info.weather_forecast()
    pprint.pprint(cty)
    pprint.pprint(forecast)


if __name__ == "__main__":
    cty = sys.argv[1]
    _main(cty)

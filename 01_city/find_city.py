# https://habr.com/ru/post/315264/
# https://home.openweathermap.org/api_keys
# http://api.openweathermap.org/data/2.5/find?q=Petersburg&type=like&APPID=a8a949143ca1b43afe204c3a86a8f906
# current weather request:  http://api.openweathermap.org/data/2.5/weather?id=472045&APPID=a8a949143ca1b43afe204c3a86a8f906
# forecast request:  http://api.openweathermap.org/data/2.5/forecast?id=472045&APPID=a8a949143ca1b43afe204c3a86a8f906
# City: Voronezh  Id: 472045

import sys
import requests
from dateutil.parser import parse

class City:
    def __init__(self, s_city):
        self.s_city = s_city

    def find_city(self):
        appid = "a8a949143ca1b43afe204c3a86a8f906"
        res = requests.get("http://api.openweathermap.org/data/2.5/find",
                        params={'q': self.s_city, 'type': 'like', 'units': 'metric', 'APPID': appid})
        data = res.json()
        for i in data['list']:
            print(i['id'], i['name'], i['coord'], i['sys'])


def _main(city):
    city = City(city)
    city.find_city()

if __name__ == "__main__":
    city = sys.argv[1]
    _main(city)

##########
city_dict = {
    "Voronezh": "472045",
    "Kazan": "551487",
    "": ""
}




# def get_current():
#     s_city = "Voronezh"
#     city_id = "472045"
#     appid = "a8a949143ca1b43afe204c3a86a8f906"
#     res = requests.get("http://api.openweathermap.org/data/2.5/weather",
#                     params={'id': city_id, 'units': 'metric', 'APPID': appid})
#     data = res.json()
#     print(data)

# def get_forecast():
#     s_city = "Voronezh"
#     city_id = "472045"
#     appid = "a8a949143ca1b43afe204c3a86a8f906"
#     res = requests.get("http://api.openweathermap.org/data/2.5/forecast",
#                            params={'id': city_id, 'units': 'metric', 'APPID': appid})
#     data = res.json()
#     forecast = list()
#     for i in data['list']:
#             if(i['dt_txt'].split()[1]) == "12:00:00":
#                 elem = i['dt_txt'].split()[0], i['main']['temp']
#                 el = {
#                     "date": parse(i['dt_txt'].split()[0]),
#                     "temp": i['main']['temp']
#                 }
#                 #print(elem)
#                 forecast.append(el)
#     print(forecast)


#get_current()
#get_forecast()
